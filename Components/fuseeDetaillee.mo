model fuseeDetaillee


  extends fusee_partielle

  parameter Real waterFill = 0.55"(min=0, max=1) water filling fraction";
  parameter Real h_ini_eau = hauteur_fusee*waterFill;
  parameter Real p_ext = 1e5;
  parameter Real h_f = 2*hauteur_fusee*(P-p_ext)/(gamma*P);
  parameter Real tho = ((rho_eau*(P-p_ext)/2)^(1/2))*2*hauteur_fusee*rocket_area/(nozzle_area*gamma*P);
  parameter Real m_fusee_ini = h_ini_eau*rocket_area*rho_eau + mBody;


// Variables
  

  Modelica.Blocks.Interfaces.RealOutput p_air(unit="bar") "pression de l'air dans la bouteille" ;
  Modelica.Blocks.Interfaces.RealOutput h_eau(unit="m") "hauteur de l'eau dans la bouteille" ;
  Modelica.Blocks.Interfaces.RealOutput v_eau(unit="m/s") "vitesse de l'eau en sortie de la bouteille" ;
  Modelica.Blocks.Interfaces.RealOutput m_fusee(unit="kg") "masse de la fusee";
  Modelica.Blocks.Interfaces.RealOutput debit_massique(unit="kg/s") "debit massique de l'eau" ;
  Modelica.Blocks.Interfaces.RealOutput trainee(unit="N") "force de trainee provoquee par les frottements" ;
  Modelica.Blocks.Interfaces.RealOutput poussee(unit="N") "force de poussee generee par la propulsion" ;
  

initial equation
  m_fusee = m_fusee_ini;
  


equation
  h_eau = -h_f + (h_ini_eau + h_f)*exp(-time/tho);
  p_air = P*((hauteur_fusee-h_ini_eau)/(hauteur_fusee-h_eau))^gamma;
  v_eau = -der(h_eau)*rocket_area/nozzle_area;
  debit_massique = v_eau*nozzle_area*rho_eau;
  m_fusee = m_fusee_ini - debit_massique*time;
  v=der(z);
  trainee = 0.5*Cd*rocket_area*densite_air*v^2;
  poussee = rho_eau*nozzle_area*v_eau^2;
  m_fusee*a = poussee - m_fusee*9.80 - trainee;
  a=der(v);

end fuseeDetaillee;

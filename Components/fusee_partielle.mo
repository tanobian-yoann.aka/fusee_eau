partial model fusee_partielle

// Variables
  
  Modelica.Blocks.Interfaces.RealOutput z(unit="m") "altitude" annotation(
    Placement(visible = true, transformation(origin = {110, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput v(unit="m/s") "velocity" annotation(
    Placement(visible = true, transformation(origin = {110, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput a(unit="m/s^2") "accelaration" ;
  
  Real poussee "Poussée produite par la fusée";
  Real trainee "Traînée sur la fusée";
  
  //Parametres
    
  parameter Real densite_air = 1.225 "Densite de l'air";
  parameter Modelica.Units.SI.Mass mBody(min=0, displayUnit="g") = 56e-3 "mass of the rocket body"; /* 56 g = 34 g for bottle + 22 g for cap with nozzle and fins */
  parameter Real waterFill(min=0, max=1) "water filling fraction";
  parameter Real Cd = 0.4 "drag coefficient";
  parameter Real gamma = 1.4 "Rapport des capacités thermiques";
  parameter Real Z = 1 "Coefficient de compressibilité";
  parameter Real R = 8.314 "Constante universelle des gaz";
  parameter Real M = 0.028 "Masse molaire de l'air";
  parameter Real Ta = 300 "Température absolue à l'admission";
  
  parameter Real volume_bouteille = 0.0011 "Volume de la bouteille";
  parameter Real hauteur_fusee = 0.035 "hauteur fusee";
 
  parameter Real nozzle_area = 0.000314 "Area of the nozzle";
  parameter Real rocket_area = 0.0314 "Cross-sectional area of the rocket";
  parameter Real densite_air = 1.225 "Density of air";
  parameter Real rho_eau = 1000 "masse volumique de l'eau";
  parameter Modelica.Units.SI.Pressure P(displayUnit="bar") = 4e5 "compressed air pressure at liftoff";
  
  
initial equation
  z = 0;
  
  

equation
  /* Equations which are common to all rocket models */
  
  trainee = 0.5*Cd*rocket_area*densite_air*v^2;
  
  m_fusee*a = poussee - m_fusee*9.80 - trainee;
  
  // Résoudre pour la position et la vitesse
  der(v) = a;
  der(z) = v;

annotation(
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Polygon(origin = {0, 20}, fillColor = {222, 221, 218}, fillPattern = FillPattern.Solid, points = {{-10, -60}, {-30, 20}, {-30, 40}, {-10, 60}, {10, 60}, {30, 40}, {30, 20}, {10, -60}, {-10, -60}}), Polygon(origin = {-20, -60}, fillColor = {246, 97, 81}, fillPattern = FillPattern.Solid, points = {{-10, -20}, {10, 20}, {10, -20}, {-10, -20}}), Polygon(origin = {20, -60}, fillColor = {246, 97, 81}, fillPattern = FillPattern.Solid, points = {{10, -20}, {-10, 20}, {-10, -20}, {10, -20}}), Text(origin = {0, -120}, textColor = {28, 113, 216}, extent = {{-100, 20}, {100, -20}}, textString = "%name"), Text(origin = {0, 40}, extent = {{-50, 20}, {50, -20}}, textString = "%P")}, coordinateSystem(extent = {{-100, -100}, {100, 100}})));


end fusee_partielle;
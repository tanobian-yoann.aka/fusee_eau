model fusee_simple
  
  extends fusee_partielle

  parameter Real v_air = 451.7 ;

  
initial equation
  v = v_air;
  m_fusee = mBody
  

equation
  /* Equations which are common to all rocket models */
  
  trainee = 0.5*Cd*rocket_area*densite_air*v^2;
  debit_massique = v_air *nozzle_area*densite_air;
  der(m_fusee) = 0
  poussee = densite_air*nozzle_area*v_air^2;
  
  

end fusee_simple;
